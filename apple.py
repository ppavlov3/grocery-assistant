"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""


import termcolor


def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))

    if n <= 0 or n > 30:
        print("Столько нет")
    elif (n in range(11, 15) or n % 10 == 0 or n % 10 in range(5, 10)):
        print("Пожалуйста,", n, "яблок")
    elif n % 10 == 1:
        print("Пожалуйста,", n, "яблоко")
    elif n % 10 in range(2, 5):
        print("Пожалуйста,", n, "яблока")


if __name__ == "__main__":
    print(termcolor.colored("Grocery assistant", "green"))  # color this caption
    main()
